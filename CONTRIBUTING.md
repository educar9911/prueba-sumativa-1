print("\n\t=================================="
      "\n\t PROGRAMA DE SQLITE3 BASE DE DATOS"
      "\n\t CARLOS CORDOBA PIZARRO - II AÑO"
      "\n\t UNIVERSIDAD LATINA DE PANAMA 2020"
      "\n\t==================================\n\t")
opc = 0
import sqlite3

#CREAR UNA TABLA
miconexion = sqlite3.connect("SLANG.db")
miconsulta = miconexion.cursor()
sql = """
CREATE TABLE IF NOT EXISTS Palabras(
Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
Palabra VARCHAR(20) NOT NULL,
Significado VARCHAR(60) NOT NULL)
"""
#COMPROBAR SI LA TABLA EXISTE
if(miconsulta.execute(sql)):
    print("TABLA CREADA")
else:
    print("ERROR AL CREAR LA TABLA")
miconsulta.close()
miconexion.commit()
miconexion.close()
# FIN DE CREAR TABLA

def insertar():
    miconexion = sqlite3.connect("SLANG.db")
    miconsulta = miconexion.cursor()

    # INSERTAR DATOS EN LA TABLA
    Palabra = str(input("\tDigite una palabra: "))
    Significado = str(input("\tDigite el significado de la palabra: "))
    datos = (Palabra, Significado)
    sql = "INSERT INTO Palabras(Palabra,Significado) VALUES(?,?)"

    # CONSULTAR SI SE GUARDARON LOS DATOS
    if (miconsulta.execute(sql, datos)):
        print("Datos guardados correctamente")
    else:
        print("Error al guardar los datos")
    miconsulta.close()
    miconexion.commit()
    miconexion.close()
# FIN DE INSERTAR


def modificar():
    miconexion = sqlite3.connect("SLANG.db")
    miconsulta = miconexion.cursor()
    # MODIFICAR DATOS DE LA TABLA
    ModiPalabra = str(input("\tDigite la palabra a modificar: "))
    NuevaPalabra = str(input("\tDigite la nueva palabra: "))
    NuevoSignificado = str(input("\tDigite el significado: "))
    miconsulta.execute("UPDATE Palabras SET Palabra = 'NuevaPalabra' WHERE Palabra = 'ModiPalabra'")
    miconsulta.execute("UPDATE Palabras SET Significado = 'NuevoSignificado' WHERE Palabra = 'ModiPalabra'")
    miconsulta.close()
    miconexion.commit()
    miconexion.close()

    # CONSULTAR DATOS EN LA TABLA
    miconexion = sqlite3.connect("SLANG.db")
    miconsulta = miconexion.cursor()
    # miconsulta.execute("SELECT Id,Palabra,Significado FROM Palabras")
    BuscaPalabra = str(input("\tDigite la palabra a buscar: "))
    miconsulta.execute("SELECT Id,Palabra,Significado FROM Palabras WHERE Palabra = BuscaPalabra")
    print("ID   PALABRA     SIGNIFICADO")
    for i in miconsulta:
        print("", i[0], " ", i[1], "   ", i[2])
    miconsulta.close()
    miconexion.commit()
    miconexion.close()
# FIN DE MODIFICAR

def eliminar():
    # DICCIONARIO DE SLANG
    import sqlite3

    # CREAR UNA CONEXION A LA BASE DE DATOS
    miconexion = sqlite3.connect("SLANG.db")
    # CREAR UN CURSOR
    miconsulta = miconexion.cursor()

    # BORRAR DATOS DE LA TABLA
    miconsulta.execute("DELETE FROM Palabras WHERE Id = 9")
    miconsulta.close()
    miconexion.commit()
    miconexion.close()

    # CREAR UNA CONEXION A LA BASE DE DATOS
    miconexion = sqlite3.connect("SLANG.db")
    # CREAR UN CURSOR
    miconsulta = miconexion.cursor()

    # CONSULTAR DATOS EN LA TABLA
    miconsulta.execute("SELECT Id,Palabra,Significado FROM Palabras")
    # miconsulta.execute("SELECT Id,Palabra,Significado FROM Palabras WHERE Id = 2")
    print("ID   PALABRA     SIGNIFICADO")
    for i in miconsulta:
        print("", i[0], " ", i[1], "   ", i[2])
    miconsulta.close()
    miconexion.commit()
    miconexion.close()
# FIN DE ELIMINAR

def VerListado():
    # DICCIONARIO DE SLANG
    import sqlite3

    # CREAR UNA CONECCION
    miconexion = sqlite3.connect("SLANG.db")
    # CREAR UN CURSOR
    miconsulta = miconexion.cursor()

    # CONSULTAR DATOS EN LA TABLA
    miconsulta.execute("SELECT Id,Palabra,Significado FROM Palabras")
    # miconsulta.execute("SELECT Id,Palabra,Significado FROM Palabras WHERE Id = 2")
    #miconsulta.execute("SELECT Id, Palabra, Significado FROM Palabras WHERE Palabra = 'Chevere'")
    print("ID   PALABRA     SIGNIFICADO")
    for i in miconsulta:
        print("", i[0], " ", i[1], "   ", i[2])

    miconsulta.close()
    miconexion.commit()
    miconexion.close()

def buscarSignificado():
    miconexion = sqlite3.connect("SLANG.db")
    miconsulta = miconexion.cursor()
    DAT = "Xopa"
    miconsulta.execute("SELECT Id,Palabra,Significado FROM Palabras WHERE Palabra = DAT")
    print("ID   PALABRA     DESCRIPCION")
    for i in miconsulta:
        print("", i[0], " ", i[1], "   ", i[2])

    miconsulta.close()
    miconexion.commit()
    miconexion.close()



while opc != 6:
    print("\n\tMENU DE OPCIONES"
          "\n1-Agregar nueva Palabra"
          "\n2-Editar palabra existente"
          "\n3-Eliminar palabra existente"
          "\n4-Ver listado de palabras"
          "\n5-Buscar significado de palabra"
          "\n6-Salir")
    opc = int(input("\tDigite una opcion: "))
    if opc == 1:
        insertar()
    if opc == 2:
        modificar()
    if opc == 3:
        eliminar()
    if opc == 4:
        VerListado()
    if opc == 5:
        buscarSignificado()

print("\n\tFin del Programa")
